# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kdeplasma-addons package.
#
# giovanni <g.sora@tiscali.it>, 2023, 2024.
msgid ""
msgstr ""
"Project-Id-Version: kdeplasma-addons\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-03-11 00:38+0000\n"
"PO-Revision-Date: 2024-03-11 11:55+0100\n"
"Last-Translator: giovanni <g.sora@tiscali.it>\n"
"Language-Team: Interlingua <kde-i18n-doc@kde.org>\n"
"Language: ia\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.12.3\n"

#: package/contents/config/config.qml:12
#, kde-format
msgctxt "@title"
msgid "General"
msgstr "General"

#: package/contents/config/config.qml:18
#, kde-format
msgctxt "@title"
msgid "Appearance"
msgstr "Apparentia"

#: package/contents/ui/ConfigAppearance.qml:33
#, kde-format
msgctxt "@title:group"
msgid "Icon:"
msgstr "Icone:"

#: package/contents/ui/ConfigAppearance.qml:34
#, kde-format
msgctxt "@option:radio"
msgid "Website's favicon"
msgstr "Favicon de Sito Web"

#: package/contents/ui/ConfigAppearance.qml:57
#, kde-format
msgctxt "@action:button"
msgid "Change Web Browser's icon"
msgstr "Cambia Icone de Navigator de Web"

#: package/contents/ui/ConfigAppearance.qml:58
#, kde-format
msgctxt "@info:whatsthis"
msgid ""
"Current icon is %1. Click to open menu to change the current icon or reset "
"to the default icon."
msgstr ""
"Icone currente es %1. Pulsa pro aperir menu per cambiar le icone currente o "
"refixar al icone predefinite."

#: package/contents/ui/ConfigAppearance.qml:62
#, kde-format
msgctxt "@info:tooltip"
msgid "Icon name is \"%1\""
msgstr "Nomine de icone es \"%1\""

#: package/contents/ui/ConfigAppearance.qml:97
#, kde-format
msgctxt "@item:inmenu Open icon chooser dialog"
msgid "Choose…"
msgstr "Selige..."

#: package/contents/ui/ConfigAppearance.qml:99
#, kde-format
msgctxt "@info:whatsthis"
msgid "Choose an icon for Web Browser"
msgstr "Selige un icone per Navigator de Web"

#: package/contents/ui/ConfigAppearance.qml:103
#, kde-format
msgctxt "@item:inmenu Reset icon to default"
msgid "Reset to default icon"
msgstr "Refixa a icone predfinite"

#: package/contents/ui/ConfigAppearance.qml:118
#, kde-format
msgctxt "@option:check"
msgid "Display Navigation Bar"
msgstr "MOnstra Barra de navigation"

#: package/contents/ui/ConfigGeneral.qml:21
#, kde-format
msgctxt "@option:radio"
msgid "Load last-visited page"
msgstr "Carga ultime pagina visitate"

#: package/contents/ui/ConfigGeneral.qml:25
#, kde-format
msgctxt "@title:group"
msgid "On startup:"
msgstr "Al initio:"

#: package/contents/ui/ConfigGeneral.qml:32
#, kde-format
msgctxt "@option:radio"
msgid "Always load this page:"
msgstr "Sempre carga iste pagina"

#: package/contents/ui/ConfigGeneral.qml:64 package/contents/ui/main.qml:106
#, kde-format
msgctxt "@info"
msgid "Type a URL"
msgstr "Typa un URL"

#: package/contents/ui/ConfigGeneral.qml:75
#, kde-format
msgctxt "@title:group"
msgid "Content scaling:"
msgstr "Scalar de contento:"

#: package/contents/ui/ConfigGeneral.qml:79
#, kde-format
msgctxt "@option:radio"
msgid "Fixed scale:"
msgstr "Scala fixate"

#: package/contents/ui/ConfigGeneral.qml:113
#, kde-format
msgctxt "@option:radio"
msgid "Automatic scaling if width is below"
msgstr "Scalar automatic si largessa es in basso"

#: package/contents/ui/main.qml:67
#, kde-format
msgctxt "@action:button"
msgid "Go Back"
msgstr "Vade retro"

#: package/contents/ui/main.qml:74
#, kde-format
msgctxt "@action:button"
msgid "Go Forward"
msgstr "Vade avante"

#: package/contents/ui/main.qml:82
#, kde-format
msgctxt "@action:button"
msgid "Go Home"
msgstr "Vade al Initio"

#: package/contents/ui/main.qml:87
#, kde-format
msgid "Open Default URL"
msgstr "Aperi URLpredefinite"

#: package/contents/ui/main.qml:142
#, kde-format
msgctxt "@action:button"
msgid "Stop Loading This Page"
msgstr "Stoppa cargar iste pagina"

#: package/contents/ui/main.qml:142
#, kde-format
msgctxt "@action:button"
msgid "Reload This Page"
msgstr "Recarga iste pagina"

#: package/contents/ui/main.qml:192
#, kde-format
msgctxt "@action:inmenu"
msgid "Open Link in Browser"
msgstr "Aperi ligamine in Navigator..."

#: package/contents/ui/main.qml:198
#, kde-format
msgctxt "@action:inmenu"
msgid "Copy Link Address"
msgstr "Copia adresse de ligamine"

#: package/contents/ui/main.qml:260
#, kde-format
msgctxt "An unwanted popup was blocked"
msgid "Popup blocked"
msgstr "Popup blocate"

#: package/contents/ui/main.qml:261
#, kde-format
msgid ""
"Click here to open the following blocked popup:\n"
"%1"
msgstr ""
"Pulsa hic pro aperir le sequente popup  blocate:\n"
"%1"
